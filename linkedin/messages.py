

class Messages():

    _messages = [
"""Dear {0},
It is pleasure to e-meet you.
Currently I am looking for partners and connections across the industry. Thanks for adding me.
{1}
""",
"""Hello,
It is pleasure to e-meet you.
Currently I am looking for partners and connections across the industry. Thanks for adding me.
{0}
"""]

    def get_message(self, lead, signature):
        return self._messages[0].format(lead, signature)
    def get_simple_message(self, signature):
        return self._messages[1].format(signature)
