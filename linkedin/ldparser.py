from selenium import webdriver as wd;
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotInteractableException
from urllib import urlencode
import sys
import os
import time
import messages
import search_list_item

HOMEPAGE_URL = 'https://www.linkedin.com'
LOGIN_URL = 'https://www.linkedin.com/uas/login-submit'
MYNETWORK_URL = 'https://www.linkedin.com/mynetwork/'
SEARCH_URL = 'https://www.linkedin.com/search/results/people/?facetGeoRegion={0}&keywords={1}&origin=FACETED_SEARCH&page={2}'

ACTIONS = ['message', 'connect', 'none']
AREA = {'SZ':'["cn:8910"]', 'SH':'["cn:8909"]', 'CN':'["cn:0"]', 'BJ':'["cn:8911"]', 'HZ':'["cn:8931"]','DE':'["de:0"]', 'KR':'["kr:0"]', 'ID':'["id:0"]', 'IT':'["it:0"]', 'HK':'["hk:0"]', 'SG':'["sg:0"]', 'GB':'["gb:0"]', 'RU':'["ru:0"]'}
PAGES = 20
EXCLUDE_KEYWORDS = ['hr','talent', 'human', 'career', 'headhunt', 'recruit', 'consultant', 'health', 'wealth']


class LdParser():

    _messages = messages.Messages()

    def _exclude_position(self, position):
        for ex_word in EXCLUDE_KEYWORDS:
            if position.lower().find(ex_word.lower()) >= 0:
                return True
        return False


    def _fetch_buttons_by_text(self, buttons, text):
        i=0
        l=len(buttons)
        ret_arr = []
        while i<l:
            if buttons[i].text == text:
                ret_arr.append(buttons[i])
            i+=1
        return ret_arr

    def _fetch_button_by_text(self, buttons, text):
        i=0
        l=len(buttons)
        ret_arr = []
        while i<l:
            try:
                if buttons[i].text == text:
                    ret_arr.append(buttons[i])
            except:
                i+=1
                continue
            i+=1
        if len(ret_arr) == 1:
            return ret_arr[0]
        return None

    def __init__(self):

        self._ff = wd.Chrome()
        self._ff.get(HOMEPAGE_URL)
        self._ff.implicitly_wait(5)
        self._wait = WebDriverWait(self._ff, 10)

    def login(self, username, password):

        self._ff.find_element_by_id('login-email').send_keys(username)
        self._ff.find_element_by_id('login-password').send_keys(password)
        self._ff.find_element_by_id('login-submit').click()


    def _get_mynetwork_items(self):

        self._ff.get(MYNETWORK_URL)
        self._ff.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        time.sleep(5)

        list_items = []

        try:
            items = self._wait.until(
                        EC.presence_of_all_elements_located((By.CLASS_NAME, "mn-pymk-list__card")))
        except TimeoutException:
            print "search element not found"
            return list_items

        #making sure that all DOM spans are loaded
        names = self._wait.until(
                            EC.presence_of_all_elements_located((By.CLASS_NAME, "mn-person-info__name")))

        #making sure that all DOM buttons are loaded
        buttons = self._wait.until(
                            EC.presence_of_all_elements_located((By.XPATH, "//button")))

        print "Items found:{0}".format(str(len(items)))

        for item in items:
            list_item = search_list_item.SearchListItem()
            try:
                name = item.find_element_by_class_name('mn-person-info__name')
            except NoSuchElementException:
                print "name not found"
                continue
            list_item.Name = name.text.encode(encoding='utf8',errors='strict')
            position = item.find_element_by_class_name("mn-person-info__occupation")
            list_item.Position = position.text
            try:
                button = item.find_element_by_class_name("button-secondary-small")
                list_item.ActionButton = button
                list_item.ActionType = button.text
            except NoSuchElementException:
                print "action button not found"


            if not self._exclude_position(list_item.Position):
                list_items.append(list_item)

        return list_items



    def mynetwork(self, iterations, action):

        it=0;

        while (it<iterations):

            items = self._get_mynetwork_items()

            print('Found connections to add: ' + str(len(items)))

            for item in items:
                print "{0},{1},{2},{3}".format(
                    item.Name,
                    item.Position.encode(encoding='UTF-8',errors='strict'),
                    item.Country.encode(encoding='UTF-8',errors='strict'),
                    item.ActionType.encode(encoding='UTF-8',errors='strict'))
                if action == ACTIONS[1] and item.ActionType.lower().find(ACTIONS[1].lower()) >= 0 and item.ActionButton != None:
                    try:
                        item.ActionButton.click()
                    except:
                        print "Something wrong with the button"
                    time.sleep(2)
            it+=1

    def _get_search_list_items(self, query, area, page):

        list_items = []

        self._ff.get(SEARCH_URL.format(AREA[area], query, page))
        self._ff.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        time.sleep(5)

        try:
            items = self._wait.until(
                    EC.presence_of_all_elements_located((By.CLASS_NAME, "search-result__wrapper")))
        except TimeoutException:
            print "search element not found"
            return list_items

        #making sure that all DOM spans are loaded
        names = self._wait.until(
                    EC.presence_of_all_elements_located((By.XPATH, "//span")))

        #making sure that all DOM buttons are loaded
        buttons = self._wait.until(
                    EC.presence_of_all_elements_located((By.XPATH, "//button")))

        for item in items:
            list_item = search_list_item.SearchListItem()
            try:
                name = item.find_element_by_class_name('actor-name')
            except NoSuchElementException:
                print "name not found"
                return
            list_item.Name = name.text.encode(encoding='utf8',errors='strict')
            position = item.find_element_by_class_name("subline-level-1")
            list_item.Position = position.text
            country = item.find_element_by_class_name("subline-level-2")
            list_item.Country = country.text
            try:
                button = item.find_element_by_class_name("search-result__actions--primary")
                list_item.ActionButton = button
                list_item.ActionType = button.text
            except NoSuchElementException:
                print "action button not found"
            if not self._exclude_position(list_item.Position):
                list_items.append(list_item)

        return list_items


    def read_signature(self):
        'profile-rail-card__actor-link'
        try:
            item = self._wait.until(
                        EC.presence_of_element_located((By.CLASS_NAME, "profile-rail-card__actor-link")))
            return "{0}".format(item.text.split(' ')[0])
        except:
            print "signature is not set"
            return ""

    def search(self, query, action, area, signature):

        if area == '':
            area = 'CN'

        page = 1
        while page <= PAGES:

            items = self._get_search_list_items(query, area, page)

            if len(items) == 0:
                return

            for item in items:

                print "{0},{1},{2},{3}".format(
                    item.Name,
                    item.Position.encode(encoding='UTF-8',errors='strict'),
                    item.Country.encode(encoding='UTF-8',errors='strict'),
                    item.ActionType)

                if action == ACTIONS[0] and item.ActionType == 'Connect' and item.ActionButton != None:
                    try:
                        item.ActionButton.click()
                        buttons = self._wait.until(
                            EC.presence_of_all_elements_located((By.XPATH, "//button")))
                        b_add_note = self._fetch_button_by_text(buttons, "Add a note")

                        if b_add_note == None:
                                continue

                        b_add_note.click()
                        textareas = self._wait.until(
                            EC.presence_of_all_elements_located((By.XPATH, "//textarea")))
                        lead_name = item.Name.split(' ')[0]
                        simple_mes = self._messages.get_simple_message(signature)
                        lead_mes = self._messages.get_message(lead_name, signature)
                        try:
                            print lead_mes
                            textareas[0].send_keys(lead_mes.encode(encoding='UTF-8',errors='strict'))
                        except:
                            print simple_mes
                            textareas[0].send_keys(simple_mes)
                            print 'Name is wrong, sending simple message'

                        buttons = self._wait.until(
                            EC.presence_of_all_elements_located((By.XPATH, "//button")))

                        b_send_invitation = self._fetch_button_by_text(buttons, "Send invitation")
                        b_cancel_invitation = self._fetch_button_by_text(buttons, "Cancel")
                        try:
                            b_send_invitation.click()
                        except:
                            print "Something wrong here"
                            b_cancel_invitation.click()
                            self._ff.find_element(By.XPATH, "//button[@name='cancel']").click()

                        time.sleep(2)
                    except:
                        continue

                elif action == ACTIONS[1] and item.ActionType == 'Connect' and item.ActionButton != None:
                    item.ActionButton.click()
                    buttons = self._wait.until(
                        EC.presence_of_all_elements_located((By.XPATH, "//button")))
                    b_send_now = self._fetch_button_by_text(buttons, "Send now")
                    b_send_now.click()
                    time.sleep(2)
            page += 1
